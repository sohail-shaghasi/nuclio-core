<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core
{
	require_once('globals.hh');
	
	use nuclio\exception\NuclioException;
	use nuclio\kernel\ClassManagerInterface;
	use nuclio\core\plugin\Plugin;
	use \ReflectionClass;
	
	type ClassDef=shape
	(
		'path'			=>string,
		'name'			=>string,
		'namespace'		=>string,
		'loaded'		=>bool,
		'isSingleton'	=>bool,
		'isFactory'		=>bool,
		'isContainer'	=>bool,
		'reflection'	=>ReflectionClass,
		'instances'		=>Map<string,Vector<mixed>>
	);
	
	<<ignore>>
	class ClassManager implements ClassManagerInterface
	{
		const string EXT_PHP			='php';
		const string EXT_HACK			='hh';
		const string DEFAULT_CONTAINER	='global';
		
		private static ?ClassManager $instance=null;
		
		private Map<string,ClassDef> $registry=Map{};
		
		public static function getInstance():ClassManager
		{
			if (is_null(self::$instance))
			{
				self::$instance=new ClassManager();
			}
			return self::$instance;
		}
		
		public static function getClassInstance(string $class, /* HH_FIXME[4033] */...$args):mixed
		{
			$instance=self::getInstance()->createInstance(self::DEFAULT_CONTAINER, $class,...$args);
			return $instance;
		}
		
		public static function getContainerClassInstance(string $container,string $class, /* HH_FIXME[4033] */...$args):mixed
		{
			$instance=self::getInstance()->createInstance($container,$class,...$args);
			return $instance;
		}
		
		public function __construct()
		{
			//Register itself.
			$this->register(self::DEFAULT_CONTAINER,$this);
		}
		
		public function register(string $container,mixed $class):bool//(ClassDef $class):bool
		{
			$loaded		=false;
			$instance	=null;
			if (is_object($class))
			{
				$loaded		=true;
				$instance	=$class;
			}
			$reflection	=new ReflectionClass($class);
			$fullName	=$reflection->getName();
			
			//Has this class already been registered?
			if (!$this->registry->containsKey($fullName))
			{
				$className	=$reflection->getShortName();
				$namespace	=$reflection->getNamespaceName();
				$attributes	=$reflection->getAttributes();
				$classPath	=ROOT_DIR.str_replace('\\','/',$namespace)._DS_.$className;
				
				$isSingleton	=false;
				$isFactory		=false;
				$isContainer	=false;
				foreach ($attributes as $attributeKey=>$attributeVal)
				{
					switch ($attributeKey)
					{
						case 'singleton':
						{
							$isSingleton=true;
							break;
						}
						case 'factory':
						{
							$isFactory=true;
							break;
						}
						case 'container':
						{
							$isContainer=true;
							break;
						}
					}
				}
				if (!$isSingleton && !$isFactory)
				{
					$isSingleton=true;
				}
				
				if (is_file($classPath.'.'.self::EXT_HACK))
				{
					$classPath=realpath($classPath.'.'.self::EXT_HACK);
				}
				else if (is_file($classPath.'.'.self::EXT_PHP))
				{
					$classPath=realpath($classPath.'.'.self::EXT_PHP);
				}
				
				$classDef=shape
				(
					'path'			=>$classPath,
					'name'			=>$className,
					'namespace'		=>$namespace,
					'loaded'		=>!is_null($instance)?true:$loaded,
					'isSingleton'	=>$isSingleton,
					'isFactory'		=>$isFactory,
					'isContainer'	=>$isContainer,
					'reflection'	=>$reflection,
					'instances'		=>Map{self::DEFAULT_CONTAINER=>Vector{}},
				);
				
				if (is_object($instance))
				{
					if (!$classDef['instances']->containsKey($container))
					{
						$classDef['instances']->set($container,Vector{});
					}
					$classDef['instances'][$container][]=$instance;
				}
				$this->registry[$fullName]=$classDef;
				return true;
			}
			else
			{
				if (!$this->registry[$fullName]['instances']->containsKey($container))
				{
					$this->registry[$fullName]['instances']->set($container,Vector{});
				}
				$this->registry[$fullName]['instances'][$container][]=$instance;
			}
			return false;
		}
		
		public function isRegistered(string $class):bool
		{
			return $this->registry->containsKey($class);
		}
		
		public function createInstance(string $container,string $class, /* HH_FIXME[4033] */...$args):mixed
		{
			if ($this->registry->containsKey($class))
			{
				$classDef=$this->registry->get($class);
				if (is_null($classDef))
				{
					return null;
				}
				if ($classDef['isSingleton'])
				{
					//Yes. Is there already an instance of the specified container?
					if (count($classDef['instances']->get($container)))
					{
						//Yes. Return the instance.
						return $classDef['instances'][$container][0];
					}
				}
				else if ($classDef['isFactory'])
				{
					if ($classDef['isContainer'])
					{
						throw new NuclioException(sprintf('Unable to create new instance of class "%s" in container because class also declared itself as a factory. This behaviour is currently not supported.',$class));
					}
					$instance=newClassInstance($class,...$args);
					$this->register($container,$instance);
					return $instance;
				}
				else
				{
					//Not a singleton or a factory. Ignore it for now.
				}
			}
			$instance=newClassInstance($class,...$args);
			$this->register($container,$instance);
			return $instance;
		}
	}
}
