<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core\plugin
{
	use nuclio\Nuclio;
	use nuclio\core\ClassManager;
	use nuclio\core\Overloadable;
	use nuclio\core\plugin\PluginException;
	
	abstract class Plugin implements PluginInterface,Overloadable
	{
		// private Nuclio $nuclio;
		protected ?ClassManager $classManager=null;
		private ?string $lastKey;
		private Vector<string> $validOverloaders=Vector
		{
			'nuclio',
			'classManager',
			'plugin'
		};
		
		public function __construct()
		{
			// $nuclio::getInstance();
			// $nuclio=null;
			
			$classManager=Nuclio::getClassManager();
			
			if ($classManager instanceof ClassManager)
			{
				$this->classManager=$classManager;
			}
		}
		
		public function __call(string $method,array<mixed> $args):mixed
		{
			throw new PluginException('Overloading Exception. Undefined method "'.$method.'".');
			return null;
		}
		
		public function __get(string $key):mixed
		{
			if (in_array($key,$this->validOverloaders))
			{
				if (is_null($this->lastKey))
				{
					$this->lastKey=$key;
					return $this;
				}
				else
				{
					switch ($this->lastKey)
					{
						case 'nuclio':
						{
							
						}
						case 'classManager':
						{
							
						}
						case 'plugin':
						{
							
						}
					}
				}
			}
		}
		
		// public function getNuclio():Nuclio
		// {
		// 	return Nuclio::$INSTANCE;
		// }
	}
}
