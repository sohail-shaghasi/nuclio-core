<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core\loader
{
	require_once(__DIR__.'/globals.hh');
	
	use nuclio\Nuclio;
	use nuclio\helper\ObjectHelper;
	use nuclio\kernel\ClassLoaderInterface;
	use nuclio\core\ClassManager;
	use \ReflectionClass;

	class ClassLoader implements ClassLoaderInterface
	{
		private Map<string,mixed> $registry;
		
		const string EXT_PHP	='php';
		const string EXT_HACK	='hh';
		
		private static ?ClassLoader $instance=null;
		
		public static function getInstance():ClassLoader
		{
			if (is_null(self::$instance))
			{
				self::$instance=new self();
			}
			return self::$instance;
		}
		
		public function __construct()
		{
			$this->registry=Map{};
			self::enableAutoloader();
		}

		public function __destruct()
		{
			self::disableAutoloader();
		}
		
		public static function autoload(string $className):void
		{
			$baseClassName	=ObjectHelper::getBaseClassName($className);
			$joinedParts	=ObjectHelper::getNamespace($className)
							|>explode('\\',$$)
							|>implode(_DS_,$$);

			if ($joinedParts!='')
			{
				$joinedParts.=_DS_;
			}
			$classPath=ROOT_DIR.$joinedParts.$baseClassName;
			
			if (is_file($classPath.'.'.self::EXT_HACK))
			{
				load($classPath.'.'.self::EXT_HACK);
			}
			else if (is_file($classPath.'.'.self::EXT_PHP))
			{
				load($classPath.'.'.self::EXT_PHP);
			}
			else
			{
				self::disableAutoloader();
				$loader=composerLoad($className);
				self::enableAutoloader();
				if (!class_exists($className))
				{
					throw new ClassLoaderException('Autoload Failed :: '.$className.' :: '.$classPath.'.'.self::EXT_HACK);
					// print 'Autoload Failed :: '.$className.' :: '.$classPath.'.'.self::EXT_HACK;
					// if (function_exists('dump'))
					// {
					// 	dump(debug_backtrace());
					// }
					// else
					// {
					// 	var_dump(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT,2));
					// }
					// exit();
				}
			}
			$reflection=new ReflectionClass($className);
			$attributes=new Map($reflection->getAttributes());
			if ($reflection->isInstantiable()
			&& !$attributes->containsKey('ignore'))
			{
				Nuclio::getClassManager()->register(ClassManager::DEFAULT_CONTAINER,$className);
			}
		}

		static private function enableAutoloader():void
		{
			spl_autoload_register(array(ClassLoader::class,'autoload'),true,false);
		}

		static private function disableAutoloader():void
		{
			spl_autoload_unregister(array(ClassLoader::class,'autoload'));
		}
	}
}
