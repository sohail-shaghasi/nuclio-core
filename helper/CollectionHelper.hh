<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\helper
{
	class CollectionHelper
	{
		public static function isIndexedArray(array<mixed> $array):bool
		{
			$indexed=true;
			$keys=array_keys($array);
			for ($k=0,$l=count($keys); $k<$l; $k++)
			{
				if (is_string($keys[$k]))
				{
					$indexed=false;
					break;
				}
			}
			return $indexed;
		}
		
		public static function isKeyedArray(array<mixed> $array):bool
		{
			$keyed=false;
			$keys=array_keys($array);
			for ($k=0,$l=count($keys); $k<$l; $k++)
			{
				if (is_string($keys[$k]))
				{
					$keyed=true;
					break;
				}
			}
			return $keyed;
		}
	}
}



				
