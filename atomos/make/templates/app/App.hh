<?hh //strict
namespace {{ application.namespace }}
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\application\
	{
		application\Application,
		router\RouterException
	};
	use \Exception;
	
	<<singleton>>
	class {{ application.name }} extends Application
	{
		
		public function init():void
		{
			$this->bindRoutes();
			
			try
			{
				$this->router->execute();
			}
			catch (RouterException $exception)
			{
				if ($this->config->get('application.debug'))
				{
					debug()->handleException($exception);
				}
				
				die($exception->getCode()+' Error.');
			}
			catch (Exception $exception)
			{
				//Catch all other exceptions and handle it.
				if ($this->config->get('application.debug'))
				{
					debug()->handleException($exception);
				}
				else
				{
					die('500 Error.');
				}
			}
		}
		
		private function bindRoutes():void
		{
			
		}
	}
}