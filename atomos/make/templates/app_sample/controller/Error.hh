<?hh //strict
namespace {{ application.namespace }}\controller
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Passable;
	
	/**
	 * This Error class simple passes control to the ErrorView
	 * and specifies the appropriate method to call, which in turn
	 * will process the appropriate template and return it as a string.
	 */
	class Error extends Controller
	{
		use Passable;
		
		public function _404():void
		{
			print $this->passControlTo
			(
				'{{ application.namespace }}\\controller\\view\\ErrorView',
				'_404'
			);
		}
		
		public function _405():void
		{
			print $this->passControlTo
			(
				'{{ application.namespace }}\\controller\\view\\ErrorView',
				'_405'
			);
		}
		
		public function _500():void
		{
			print $this->passControlTo
			(
				'{{ application.namespace }}\\controller\\view\\ErrorView',
				'_500'
			);
		}
	}
}