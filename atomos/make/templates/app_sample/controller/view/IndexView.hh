<?hh //strict
namespace {{ application.namespace }}\controller\view
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Viewable;
	
	class IndexView extends Controller
	{
		use Viewable;
		
		public function run(Map<string,mixed> $data):string
		{
			/*
				This class is "Viewable" which gives us access to
				template-like methods such as setMany.
				
				setMany accepts a Map and creates template variables
				from its keyvals.
			 */
			$this->setMany($data);
			return $this->render('index.twig.tpl');
		}
	}
}