<h1>Welcome to Nuclio</h1>
<section>
	<p>This is an example app designed to get you started building your own web application!.</p>
	{% if authenticated == false %}
	<a href="/login">Demo Login Page</a>
	{% else %}
	<a href="/logout">Logout</a>
	{% endif %}
</section>