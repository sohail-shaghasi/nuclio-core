<?hh //strict
namespace {{ application.namespace }}
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\application\
	{
		application\Application,
		router\RouterException
	};
	use \Exception;
	/*
		We extend Nuclio Application to shortcut a lot of the
		inital boilerplate code required for most web applications.
		
		With this we get access to:
		* Config
		* Router
		* Request Handler
		* URI Handler
		* Crypt Helper
		
		A Nuclio Kernel will be created and bound with an autoloader,
		Class Manager, Event Manager and Exception Handler.
		
		Connections to databases defined in config will be created.
		Session will be started and managed.
		A template handler (based on config) will be initalized and load
		paths automatically bound.
		
		By specifying singleton, it informs the ClassManager that there should
		only ever be 1 instance of this class created.
	 */
	<<singleton>>
	class {{ application.name }} extends Application
	{
		/**
		 * This is a "safe" constructor and is useful for any custom
		 * bolilerplate code.
		 * 
		 * @return void
		 */
		public function init():void
		{
			//Bind all our routes for this application.
			$this->bindRoutes();
			
			/*
				Here we perform a try/catch on the router execution.
				We do this so that if there is any problem such as
				a missing route, or an incorrectly bound route,
				we can return send the request to an error page.
			 */
			try
			{
				$this->router->execute();
			}
			catch (RouterException $exception)
			{
				/*
					If we're in debug mode, we want to show
					the debug page rather than the user error page.
				 */
				if ($this->config->get('application.debug'))
				{
					debug()->handleException($exception);
				}
				/*
					We could display a generic error page, or check
					what type of error code we were given by the router
					and display an appropriate page instead.
				 */
				else switch ($exception->getCode())
				{
					//Page not found.
					case 404:	$this->router->doRoute('Error','_404');	break;
					//Method not allowed.
					case 405:	$this->router->doRoute('Error','_405');	break;
					//Server error.
					case 500:	$this->router->doRoute('Error','_500');	break;
				}
			}
			catch (Exception $exception)
			{
				//Catch all other exceptions and handle it.
				if ($this->config->get('application.debug'))
				{
					debug()->handleException($exception);
				}
				else
				{
					$this->router->doRoute('Error','_500');
				}
			}
		}
		
		/**
		 * Binds the application routes.
		 * 
		 * We bind routes to controllers. But we could choose to
		 * use any other well-known design pattern or invent something new.
		 * 
		 * We're storing all our controllers in the controllers directory.
		 * Each route must use the full namespace and our classes are
		 * following a folder -> namespace mapping convention.
		 * 
		 * @return void
		 */
		private function bindRoutes():void
		{
			/*
				We bind our default route as Index::_index
				
				The underscore is necessary due to Hack's rule which
				pervents the method name being the same as the class name.
			 */
			$this->router->newRoute('/','{{ application.namespace }}\\controller\\Index@_index','GET');
			$this->router->newRoute('/login','{{ application.namespace }}\\controller\\Index@login','GET');
			$this->router->newRoute('/logout','{{ application.namespace }}\\controller\\Index@logout','GET');
			
			/*
				This next set of routes will define our Restful API.
			 */
			$this->router->newRoute('/api/example/installTestUser','{{ application.namespace }}\\controller\\api\\Example@installTestUser','POST');
			$this->router->newRoute('/api/example/login','{{ application.namespace }}\\controller\\api\\Example@login','POST');
		}
	}
}