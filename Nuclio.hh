<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio
{
	require_once(__DIR__."/helper/DynamicHelper.hh");
	
	use nuclio\exception\NuclioException;

	use nuclio\core\ClassManager;
	use nuclio\core\request\Request;
	use nuclio\core\loader\Loader;
	use nuclio\kernel\Kernel;
	use nuclio\kernel\_empty_\Kernel as EmptyKernel;
	use nuclio\kernel\ClassManagerInterface;
	use nuclio\core\Overloadable;
	
	enum IFACE:int
	{
		CLI		=1;
		HTTP	=2;
		PHPUNIT	=3;
		BEHAT	=4;
	}
	
	enum Environment:int
	{
		PRODUCTION	=1;
		DEVELOPMENT	=2;
	}
	
	<<ignore>>
	final class Nuclio
	{
		const string VERSION					='1.5.0';
		const int VERSION_MAJOR					=1;
		const int VERSION_MINOR					=5;
		const int VERSION_PATCH					=0;
		const string VERSION_EXTRA				='release';
		const string NUCLIO_ENVIRONMENT			='NUC_ENV';
		const Environment DEFAULT_ENVIRONMENT	=Environment::PRODUCTION;
		const DEFAULT_INIT_FILE					='init.hh';
		
		private static ?Kernel $kernel;
		
		/**
		 * This kicks off Nuclio and does all the base setup of its
		 * core child classes and plugins.
		 * 
		 * Note that it will also do some initial checks of the PHP version and ensure
		 * that it is running through the HHVM parser.
		 */
		public function __construct(Kernel $kernel)
		{
			//PHP version check.
			if (!(explode('-',PHP_VERSION) |> version_compare($$[0],'5.6.99','>=')) || !strstr(PHP_VERSION,'hhvm'))
			{
				die('Nuclio requires the HHVM interpreter version 3.15.0 or higher to run.');
			}
			//Set system timezone to UTC.
			date_default_timezone_set('UTC');
			
			self::$kernel=$kernel;
		}
		
		public static function init(?string $initFile=null):void
		{
			if (is_null($initFile))
			{
				$initFile=ROOT_DIR.self::DEFAULT_INIT_FILE;
			}
			$initMap=\nuclio\helper\load($initFile);
			
			foreach ($initMap as $class=>$config)
			{
				
				if (!$config instanceof Map)
				{
					throw new NuclioException('Invalid init.hh. It must only use Maps and Vectors.');
				}
				$instanceArgs=$config->get('args') ?? Vector{};
				
				$instance=ClassManager::getClassInstance($class,...$instanceArgs);
				if ($config->get('autoInit'))
				{
					$instance->init();
				}
			}
		}
		
		public static function getClassManager():ClassManagerInterface
		{
			/*
			 * Kernel should NEVER be null. But the type checker won't
			 * allow for the case where a developer is an idiot.
			 * 
			 * Thus we do this little hack to make it happy.
			 */
			$kernel=self::$kernel;
			if (is_null($kernel))
			{
				$kernel=new EmptyKernel();
			}
			return $kernel->getClassManager();
		}
		
		public static function __callStatic(string $method,array<mixed> $args):Overloadable
		{
			if (method_exists(self::$kernel,$method))
			{
				return call_user_method_array($method,self::$kernel, $args);
			}
			else throw new NuclioException('Invalid kernel call.');
		}
	}
}
