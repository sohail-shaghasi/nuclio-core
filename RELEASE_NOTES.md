Release Notes
-------------
1.6.0
-----
* Fixed issue with inconsistent key paths for public and private keys.
* Fixed typos.
* Added new Atomos Command "make::regenInit" which regenerates the init.hh file if its out of sync.

1.5.2
-----
* Updated Atomos generated sample page.

1.5.0
-----
* Nuclio can now initalize apps by using Nuclio::init(). It accepts a path to an init.hh file but defaults to {ROOT_DIR}init.hh. It is optional to use this. However if you want to be using Atomos to generate apps, then it is required to have the app auto initialized.
* Introducing Atomos; Nuclio's new command line tool. It is slowly replacing the old "nuclio" tool. For now it comes with one sub-tool "make".


1.4.2
-----
* Removed unused/invalid global constants.
* Fixed invalid return type on MimeHelper::loadMimeList.


1.4.1
-----
* Corrected these release notes.


1.4.0
-----
* diffRequireOnceClasses in Nuclio command tool has been rewritten. It's now more reliable as it tokenizes the file. It still only reports on the first found class in the file.
* Renamed diffRequireOnceClasses to reflectClassInFile in Nuclio command tool.
* New command "db::build" for Nuclio command tool. Because JIT creation of tables is no longer supoprted. This tool will scan your project for models and create tables for them. It will also detect additions/removals of fields.
* New command "reflect" for Nuclio command tool. Useful for returning a reflection object from a new process as to not break the current process should there be a syntax error in the file.
* Cleaned up wording of some error messages in Nuclio command tool.
* Fix for CryptHelper erroring out on failure to decrypt. Now returns null instead.


1.3.0
-----

* Added a new wrapper command for PHPUnit. The command is "runTests" and it will take the same arguments as PHPUnit.
* Fixed issue with autoloader not being available if require_once had already been called.


1.2.0
-----

* New global constant NUC_DEFUALT_CONFIG_DIR. Is set to <ROOT>/config. Currently only used if you fail to pass --config to a command.
* Nuclio setup command now generates a keypair.
* Nuclio setup command has a new option for regenerating the keypair. "--regen-keypair" or "-rk".
* Using --config for the nuclio setup command is now used for generating the keypair.
* cryptHelper::generateKeypair now accepts a regen option, now returns void and is public.
* New helper method in cryptHelper "doesKeypairExist". Tests if both key files in a keypair exist. Returns 0, 1, 2 depending on if none, 1 or both exist.


1.1.1
-----
* Removed nuclio-dev from composer.json bin def.
* Fixed accidental removal of "use nuclio\Nuclio".


1.1.0
-----
* Now depending on new global constants for temp dir etc.
* Changed the way classes are reflected to not depend on the path. This has elminated the need to have a seperate dev bootstrap and CLI interface.
* The setup command will now create temp, cache and log folders, completing the setup required for Nuclio to run.
* Removed the dev bootsrap and CLI interface.



1.0.1
-----
* Fixed an issue with the composer.json bin paths.



1.0.0
-----
* Initial Release.
