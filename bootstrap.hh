<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace
{
	const string _DS_					=DIRECTORY_SEPARATOR;
	const string ROOT_DIR				=PUBLIC_DIR.'..'._DS_;
	const string NUC_DIR				=ROOT_DIR.'vendor/intaglio/nuclio-core'._DS_;
	const string NUC_BIN				=NUC_DIR.'bin'._DS_.'nuclio';
	const string NUC_TMP_DIR_NAME		='.nuclio';
	const string NUC_TMP_DIR			=ROOT_DIR.NUC_TMP_DIR_NAME._DS_;
	const string NUC_CACHE_DIR			=NUC_TMP_DIR.'cache'._DS_;
	const string NUC_LOG_DIR			=NUC_TMP_DIR.'log'._DS_;
	const string NUC_DEFUALT_CONFIG_DIR	=ROOT_DIR.'config'._DS_;
	const string NUC_PLUGIN_DIR			=NUC_DIR.'plugin'._DS_;
	const string APP_DIR				=ROOT_DIR.'app'._DS_;
	const string APP_PLUGIN_DIR			=APP_DIR.'plugin'._DS_;
	
	require_once(__DIR__.'/helper/ObjectHelper.hh');
	require_once(__DIR__.'/kernel/ClassLoaderInterface.hh');
	require_once(__DIR__.'/core/loader/ClassLoader.hh');
	nuclio\core\loader\ClassLoader::getInstance();
}
