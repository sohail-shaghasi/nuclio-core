<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\kernel
{
	use nuclio\core\exception\NuclioException;
	use nuclio\core\loader\ClassLoader;
	use nuclio\core\ClassManager;
	
	type CLIKernelParams=shape
	(
		'classLoader'		=>ClassLoader,
		'classManager'		=>ClassManager,
		'eventManager'		=>EventManagerInterface,
		'exceptionHandler'	=>ExceptionBootstrapperInterface
	);
	
	/**
	 * Nuclio's HTTP kernel class.
	 * 
	 * 
	 * @abstract
	 */
	<<ignore>>
	class CLIKernel extends Kernel
	{
		public function __construct(CLIKernelParams $params)
		{
			parent::__construct($params);
		}
	}
}
